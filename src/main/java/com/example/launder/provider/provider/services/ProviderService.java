package com.example.launder.provider.provider.services;

import com.example.launder.orders.models.Order;
import com.example.launder.orders.services.OrderService;
import com.example.launder.provider.provider.models.Provider;
import com.example.launder.provider.provider.repositories.ProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProviderService {
    @Autowired
    ProviderRepository providerRepository;
    @Autowired
    OrderService orderService;

    public Order acceptOrder(Provider provider, String orderId) {
        Order order = orderService.getOrder(orderId);
        orderService.updateOrderStateCleaner(order, 1, provider.get_id());
        return order;
    }

    public Order orderDone(Provider provider, String orderId) {
        Order order = orderService.getOrder(orderId);
        if (order.getCleanerId().equals(provider.get_id())) {
            orderService.updateOrderStateCleaner(order, 5, provider.get_id());
        }
        return order;
    }
}
