package com.example.launder.provider.provider.repositories;

import com.example.launder.provider.provider.models.Provider;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProviderRepository extends MongoRepository<Provider, String> {
    Provider findByUserId(@Param("userId") String userId);
}
