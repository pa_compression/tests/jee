package com.example.launder.orders.DTO;


import lombok.Data;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
public class OrderDTO {
    @MongoId(value = FieldType.OBJECT_ID)
    private String _id;
    private String userId;
    private Integer state;
    private String pickerId;
    private String deliverId;
    private String cleanerId;
    private Integer basketWeight;
    private Integer type;
    private Integer priceTotal;
    private Integer priceDelivery;
    private Long createdTimestamp;
    private Long updatedTimestamp;
}