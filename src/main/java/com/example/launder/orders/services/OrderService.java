package com.example.launder.orders.services;


import com.example.launder.customers.baskets.models.Basket;
import com.example.launder.customers.baskets.services.BasketService;
import com.example.launder.orders.DTO.OrderDTO;
import com.example.launder.orders.models.Order;
import com.example.launder.orders.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class OrderService {

    @Value("${app.order.delivery.price}")
    private Integer deliveryPrice;

    @Autowired
    BasketService basketService;
    @Autowired
    OrderRepository orderRepository;


    public Order createOrder(String userId, Integer type){
        Basket basket = basketService.getBasket(userId);
        if(basket != null){
            Order order = new Order();
            order.setUserId(userId);
            order.setType(type);
            order.setState(0);
            order.setBasketWeight(basket.getWeight());
            order.setPriceDelivery(calculateDeliveryPrice());
            order.setPriceTotal(calculateDeliveryPrice() + basketService.calculateBasketPrice(basket));
            order.setCreatedTimestamp(new Date().getTime());
            order.setUpdatedTimestamp(order.getCreatedTimestamp());
            return order;
        }else{
            return null;
        }
    }

    public OrderDTO OrderToDTO(Order order){
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.set_id(order.get_id());
        orderDTO.setBasketWeight(order.getBasketWeight());
        orderDTO.setCleanerId(order.getCleanerId());
        orderDTO.setDeliverId(order.getDeliverId());
        orderDTO.setPickerId(order.getPickerId());
        orderDTO.setState(order.getState());
        orderDTO.setType(order.getType());
        orderDTO.setUserId(order.getUserId());
        orderDTO.setPriceDelivery(order.getPriceDelivery());
        orderDTO.setPriceTotal(order.getPriceTotal());
        orderDTO.setCreatedTimestamp(order.getCreatedTimestamp());
        orderDTO.setUpdatedTimestamp(order.getCreatedTimestamp());
        return orderDTO;
    }

    public Integer calculateDeliveryPrice(){
        return 2*this.deliveryPrice;
    }

    public Order getOrder(String orderId){
        Order order = orderRepository.findBy_id(orderId);
        if(order != null){
            return order;
        }else{
            return null;
        }
    }

    public Order updateOrderStateCleaner(Order order, Integer state, String cleanerId) {
        order.setState(state);
        order.setCleanerId(cleanerId);
        orderRepository.save(order);
        return order;
    }

    public Order updateOrderStateDriver(Order order, Integer state, String driverId) {
        order.setState(state);
        order.setPickerId(driverId);
        orderRepository.save(order);
        return order;
    }
}
