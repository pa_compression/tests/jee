package com.example.launder.provider.address.repositories;

import com.example.launder.provider.address.models.Address;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends MongoRepository<Address, String> {
    List<Address> findByUserId(@Param("userId") String userId);
    Address findByNameAndUserId(@Param("name") String name, @Param("userId") String userId);
}
