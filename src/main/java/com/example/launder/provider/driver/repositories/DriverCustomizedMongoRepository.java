package com.example.launder.provider.driver.repositories;

import com.example.launder.commons.exceptions.ConflictException;

import java.util.Optional;

public interface DriverCustomizedMongoRepository<T, ID> {
    <S extends T> S save(S entity) throws ConflictException;
}
