package com.example.launder.customers.baskets.resources;

import com.example.launder.customers.baskets.DTO.BasketDTO;
import com.example.launder.customers.baskets.models.Basket;
import com.example.launder.customers.baskets.repositories.BasketRepository;
import com.example.launder.customers.baskets.services.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/baskets")
public class BasketResource {

    @Autowired
    BasketRepository basketRepository;
    private final BasketService basketService;

    public BasketResource(BasketService basketService) { this.basketService = basketService; }

    @GetMapping("/{userId}")
    public ResponseEntity<?> getBasket(@PathVariable("userId") String userId){
        Basket basket = basketService.getBasket(userId);
        if(basket != null){
            return new ResponseEntity<>(basketService.BasketToDTO(basket), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/history/{userId}")
    public ResponseEntity<?> getBasketsHistory(@PathVariable("userId") String userId){
        List<Basket> basketsHistory = basketRepository.findAllByUserIdOrderByCreatedTimestampDesc(userId);
        for(int i = 0; i < basketsHistory.size(); i++) basketsHistory.get(i).setPrice(basketService.calculateBasketPrice(basketsHistory.get(i)));
        return new ResponseEntity<>(basketsHistory, HttpStatus.OK);
    }

    @PutMapping("/{userId}")
    public ResponseEntity<?> updateBasketWeight(@PathVariable("userId") String userId, @RequestBody Integer weight){
        Basket basket = basketRepository.findByUserId(userId);
        if(basket != null){
            basket.setWeight(weight);
            basketRepository.save(basket);
            basket.setPrice(basketService.calculateBasketPrice(basket));
            return new ResponseEntity<>(basketService.BasketToDTO(basket), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
