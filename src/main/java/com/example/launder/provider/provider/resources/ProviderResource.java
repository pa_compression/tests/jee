package com.example.launder.provider.provider.resources;

import com.example.launder.orders.models.Order;
import com.example.launder.provider.provider.models.Provider;
import com.example.launder.provider.provider.repositories.ProviderRepository;
import com.example.launder.provider.provider.services.ProviderService;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/providers")
public class ProviderResource {
    @Autowired
    ProviderService providerService;
    @Autowired
    ProviderRepository providerRepository;

    @PutMapping("/order/{orderId}/{accepted}/{userId}")
    public ResponseEntity<?> acceptOrder(@PathVariable String orderId, @PathVariable Boolean accepted, @PathVariable String userId) {
        Provider provider = providerRepository.findByUserId(userId);
        if (provider == null) {
            JSONObject response = new JSONObject();
            response.put("message", "Provider not found");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        if(accepted) {
            Order order = providerService.acceptOrder(provider, orderId);
            return new ResponseEntity<>(order, HttpStatus.OK);
        }
        JSONObject response = new JSONObject();
        response.put("message", "Order not accepted");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/orderDone/{orderId}/{userId}")
    public ResponseEntity<?> orderDone(@PathVariable String orderId, @PathVariable String userId) {
        Provider provider = providerRepository.findByUserId(userId);
        if (provider == null) {
            JSONObject response = new JSONObject();
            response.put("message", "Provider not found");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        Order order = providerService.orderDone(provider, orderId);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }
}
