package com.example.launder.provider.driver.repositories;

import com.example.launder.commons.exceptions.ConflictException;
import com.example.launder.provider.driver.models.Driver;
import com.example.launder.provider.user.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

public class DriverCustomizedMongoRepositoryImpl<T, ID> implements DriverCustomizedMongoRepository<T, ID> {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public <S extends T> S save(S entity) throws ConflictException {
        Driver driver = (Driver) entity;
        System.out.println(driver);
        User user = mongoTemplate.findById(driver.getUserId(), User.class, "user");
        if (user != null) {
            throw new ConflictException("A driver is already associated with this user");
        }
        mongoTemplate.save(driver, "driver");
        return (S) driver;
    }
}
