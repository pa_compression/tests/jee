package com.example.launder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LaunderApplication {
    public static void main(String[] args) {
        SpringApplication.run(LaunderApplication.class, args);
    }
}
