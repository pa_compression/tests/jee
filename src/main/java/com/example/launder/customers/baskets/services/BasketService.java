package com.example.launder.customers.baskets.services;


import com.example.launder.customers.baskets.DTO.BasketDTO;
import com.example.launder.customers.baskets.models.Basket;
import com.example.launder.customers.baskets.repositories.BasketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BasketService {

    @Autowired
    BasketRepository basketRepository;

    @Value("${app.basket.content.weight}")
    private Integer weight;

    public Boolean createBasket(String userId){
        Basket hasACurrentBasket = basketRepository.findByUserId(userId);
        if(hasACurrentBasket == null){
            Basket basket = new Basket();
            basket.setWeight(0);
            basket.setUserId(userId);
            basket.setCreatedTimestamp(new Date().getTime());
            basketRepository.save(basket);
            return true;
        }else{
            return false;
        }
    }

    public Basket getBasket(String userId){
        Basket basket = basketRepository.findByUserId(userId);
        if(basket != null) {
            return basket;
        }else{
            return null;
        }
    }

    public Integer calculateBasketPrice(Basket basket){
        Integer price = 0;
        price = basket.getWeight() * this.weight;
        return price;
    }


    public BasketDTO BasketToDTO(Basket basket){
        BasketDTO basketDTO = new BasketDTO();
        basketDTO.set_id(basket.get_id());
        basketDTO.setWeight(basket.getWeight());
        basketDTO.setUserId(basket.getUserId());
        basketDTO.setPrice(basket.getPrice() == null ? 0 : basket.getPrice());
        basketDTO.setCreatedTimestamp(basket.getCreatedTimestamp());
        return basketDTO;
    }
}
