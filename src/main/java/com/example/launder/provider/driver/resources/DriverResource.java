package com.example.launder.provider.driver.resources;

import com.example.launder.commons.exceptions.ConflictException;
import com.example.launder.orders.models.Order;
import com.example.launder.provider.driver.services.DriverService;
import com.example.launder.provider.driver.models.Driver;
import com.example.launder.provider.driver.repositories.DriverRepository;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/drivers")
public class DriverResource {
    @Autowired
    DriverRepository driverRepository;
    private final DriverService driverService;

    public DriverResource(DriverService driverService) {
        this.driverService = driverService;
    }

    @GetMapping("/available")
    public ResponseEntity<?> getAvailableDrivers() {
        List<Driver> drivers = driverRepository.findAllByAvailable(true, PageRequest.of(0, 10, Sort.by(Sort.Order.asc("_id"))));
        driverService.updateConfirmationStatus(drivers);
        if (drivers.size() != 0) {
            return new ResponseEntity<>(drivers, HttpStatus.OK);
        } else {
            JSONObject response = new JSONObject();
            response.put("message", "No available drivers");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/waiting/confirmation")
    public ResponseEntity<?> isWaiting(@Param("userId") String userId) {
        Driver driver = driverRepository.findByUserId(userId);
        JSONObject response = new JSONObject();
        if (driver != null) {
            response.put("waiting", driver.isWaitingForConfirmation());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        else {
            response.put("message", "Driver not found");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
    }

    // TODO: Add available control
    @PutMapping("/confirm/{userId}/{confirmed}/{orderId}")
    public ResponseEntity<?> confirm(@PathVariable String userId, @PathVariable boolean confirmed, @PathVariable String orderId) throws ConflictException {
        System.out.println(userId);
        Driver driver = driverRepository.findByUserId(userId);
        if (driver == null) {
            JSONObject response = new JSONObject();
            response.put("message", "Driver not found");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        if (confirmed && driver.isWaitingForConfirmation()) {
            driverService.updateAvailable(driver);
            driverService.orderAccepted(driver, orderId);
        }
        driverService.updateConfirmation(driver);
        return new ResponseEntity<>(driver, HttpStatus.OK);
    }

    @PutMapping("/orderPickedUp/{userId}/{orderId}")
    public ResponseEntity<?> orderPickedUp(@PathVariable String userId, @PathVariable String orderId) {
        Driver driver = driverRepository.findByUserId(userId);
        if (driver == null) {
            JSONObject response = new JSONObject();
            response.put("message", "Driver not found");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        Order order = driverService.orderPickedUp(driver, orderId);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PutMapping("/orderDeliveredToCleaner/{userId}/{orderId}")
    public ResponseEntity<?> orderDeliveredToCleaner(@PathVariable String userId, @PathVariable String orderId) throws ConflictException {
        Driver driver = driverRepository.findByUserId(userId);
        if (driver == null) {
            JSONObject response = new JSONObject();
            response.put("message", "Driver not found");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        Order order = driverService.orderDeliveredCleaner(driver, orderId);
        driverService.updateAvailable(driver);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PutMapping("/orderDonePickedUp/{userId}/{orderId}")
    public ResponseEntity<?> orderDonePickedUp(@PathVariable String userId, @PathVariable String orderId) {
        Driver driver = driverRepository.findByUserId(userId);
        if (driver == null) {
            JSONObject response = new JSONObject();
            response.put("message", "Driver not found");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        Order order = driverService.orderDonePickedUp(driver, orderId);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PutMapping("/orderDoneDelivered/{userId}/{orderId}")
    public ResponseEntity<?> orderDoneDelivered(@PathVariable String userId, @PathVariable String orderId) throws ConflictException {
        Driver driver = driverRepository.findByUserId(userId);
        if (driver == null) {
            JSONObject response = new JSONObject();
            response.put("message", "Driver not found");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        Order order = driverService.orderDoneDelivered(driver, orderId);
        driverService.updateAvailable(driver);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }
}
