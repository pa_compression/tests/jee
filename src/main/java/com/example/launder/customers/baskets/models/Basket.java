package com.example.launder.customers.baskets.models;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.List;

@Data
public class Basket {
    @MongoId(value = FieldType.OBJECT_ID)
    private String _id;
    private String userId;
    private Integer weight;
    private Integer price;
    private Long createdTimestamp;

}
