package com.example.launder.provider.provider.models;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
public class Provider {
    @MongoId(value = FieldType.OBJECT_ID)
    private String _id;
    private String userId;
    private double rate;
    private String type;
}
